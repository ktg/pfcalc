# pfcalc: Postfix Calculator #

(C) 2013 Kim, Taegyoon

## Operators ##
```
+ - * / ^ % sqrt inc dec ++ -- floor ceil ln log10 e pi +' *'
== != < > <= >= && ||
if when for while
dup swap pop stack->string
'string [string] strlen strcat char-at chr
string double type eval-string get set
pr prn clear exit
; end-of-line comment
```

## Examples ##
### Hello, World! ###
```
[Hello, World!] prn
```

### [Project Euler Problem 1](http://projecteuler.net/problem=1) ###
```
0 's set
  'i 1 999 1
  (i 3 % 0 ==
  i 5 % 0 == ||
  (s i + 's set) when) for
s prn
```
=> 233168

### [Project Euler Problem 2](http://projecteuler.net/problem=2) ###
```
1 'a set
1 'b set
0 'sum set
  (a 4000000 <=)
  (a b + 'c set
  b 'a set
  c 'b set
  a 2 % 0 ==
  (sum a + 'sum set) when) while
sum prn
```
=> 4613732

### [Project Euler Problem 3](http://projecteuler.net/problem=3) ###
```
600851475143 'num set
1 'p set
  (num 1 >)
  ('p ++
  (num p % 0 ==)
    (num p / 'num set) while)
  while
p prn
```
=> 6857

### [Project Euler Problem 4](http://projecteuler.net/problem=4) ###
```
0 'maxP set
'i 100 999 1 (
  'j 100 999 1 (
    i j * 'p set
    p string 'ps set
    ps strlen 'len set
    len 2 / 'to set
    1 'pal set
    0 'k set
    len dec 'k2 set
      (k to < pal &&)
      (ps k char-at
      ps k2 char-at != (0 'pal set) when
      'k ++
      'k2 --) while
    pal (p maxP > (p 'maxP set) when) when) for) for
maxP prn
```
=> 906609

### [99 Bottles of Beer](http://en.wikipedia.org/wiki/99_Bottles_of_Beer) ###
```
'i 99 1 -1 
  (i pr
  [ bottles of beer on the wall, ] pr
  i pr
  [ bottles of beer.] prn
  [Take one down and pass it around, ] pr
  i dec pr
  [ bottle of beer on the wall.] prn) for
```
