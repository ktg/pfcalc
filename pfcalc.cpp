// (C) 2013 Kim, Taegyoon
// Postfix Calculator

#include <iostream>
#include <sstream>
#include <cstdio>
#include <vector>
#include <cstdlib>
#include <cmath>
#include <unordered_map>
#include <ctime>
using namespace std;

inline void print_logo() {
    puts(
"pfcalc: Postfix Calculator (C) 2013 Kim, Taegyoon\n\
Press Enter key twice to evaluate.\n\
Operators:\n\
+ - * / ^ % sqrt inc dec ++ -- floor ceil ln log10 e pi rand +' *'\n\
== != < > <= >= && ||\n\
if when for while\n\
dup swap pop stack->string\n\
'string [string] eval-string strlen strcat char-at chr\n\
string double parse type get set\n\
(list) eval-list\n\
pr prn clear exit\n\
; end-of-line comment");
}

struct node {
    enum {T_NIL, T_DOUBLE, T_STRING, T_BUILTIN, T_VAR, T_LIST} type;
    enum builtin {PLUS, MINUS, MUL, DIV, CARET, PERCENT, SQRT, INC, DEC, PLUSPLUS, MINUSMINUS, FLOOR, CEIL, LN, LOG10, E, PI, RAND, PLUSQ, MULQ,
        EQEQ, NOTEQ, LT, GT, LTE, GTE, ANDAND, OROR,
        IF, WHEN, FOR, WHILE,
        DUP, SWAP, POP, STACK2STRING,
        EVAL_STRING, STRLEN, STRCAT, CHAR_AT, CHR,
        STRING, DOUBLE, PARSE, TYPE, GET, SET,
        EVAL_LIST,
        PR, PRN, CLEAR, EXIT};
    union {
        double v_double;
        int v_builtin;        
    };
    string v_string;
    vector<node> v_list;

    inline node(double d): type(T_DOUBLE), v_double(d) {
    }
    inline node(string &s): type(T_STRING), v_string(s) {
    }
    inline node(vector<node> &lst): type(T_LIST), v_list(lst) {
    }

    inline static node builtin(int b) {
        node n;
        n.type = node::T_BUILTIN;
        n.v_builtin = b;
        return n;
    }
    inline double dbl() { // convert to double
        switch (type) {
        case node::T_DOUBLE:
            return v_double;
        default:
            return atof(v_string.c_str());
        }
    }
    inline string str() { // convert to string
        stringstream ss;
        ss.precision(20);
        switch (type) {
        case node::T_DOUBLE:
            ss << v_double; break;
        case node::T_STRING:
            ss << v_string; break;
        case node::T_LIST:
            {
                ss << '(';
                int size = v_list.size();
                int i = 0;
                for (vector<node>::iterator iter = v_list.begin(); iter != v_list.end(); iter++) {
                    ss << iter->str();                    
                    if (++i != size) ss << ' ';
                }
                ss << ')';
                break;
            }
        }
        return ss.str();
    }
    inline string type_str() {
        switch (type) {
        case node::T_NIL:
            return "nil";
        case node::T_DOUBLE:
            return "double";
        case node::T_STRING:
            return "string";
        case node::T_LIST:
            return "list";
        default:
            return "invalid type";
        }
    }
    inline string str_with_type() {
        return str() + ' ' + type_str();
    }
    node(): type(T_NIL) {}
};

inline double rand_double() {
    return (double) rand() / ((double) RAND_MAX + 1.0);
}

unordered_map<string, int> builtin_map;

void init() {
    srand((unsigned int) time(0));
    builtin_map["+"] = node::PLUS;
    builtin_map["-"] = node::MINUS;
    builtin_map["*"] = node::MUL;
    builtin_map["/"] = node::DIV;
    builtin_map["^"] = node::CARET;
    builtin_map["%"] = node::PERCENT;
    builtin_map["sqrt"] = node::SQRT;
    builtin_map["inc"] = node::INC;
    builtin_map["dec"] = node::DEC;
    builtin_map["++"] = node::PLUSPLUS;
    builtin_map["--"] = node::MINUSMINUS;
    builtin_map["floor"] = node::FLOOR;
    builtin_map["ceil"] = node::CEIL;
    builtin_map["ln"] = node::LN;
    builtin_map["log10"] = node::LOG10;
    builtin_map["e"] = node::E;
    builtin_map["pi"] = node::PI;
    builtin_map["rand"] = node::RAND;
    builtin_map["+'"] = node::PLUSQ;
    builtin_map["*'"] = node::MULQ;
    builtin_map["=="] = node::EQEQ;
    builtin_map["!="] = node::NOTEQ;
    builtin_map["<"] = node::LT;
    builtin_map[">"] = node::GT;
    builtin_map["<="] = node::LTE;
    builtin_map[">="] = node::GTE;
    builtin_map["&&"] = node::ANDAND;
    builtin_map["||"] = node::OROR;
    builtin_map["if"] = node::IF;
    builtin_map["when"] = node::WHEN;
    builtin_map["for"] = node::FOR;
    builtin_map["while"] = node::WHILE;
    builtin_map["dup"] = node::DUP;
    builtin_map["swap"] = node::SWAP;
    builtin_map["pop"] = node::POP;
    builtin_map["stack->string"] = node::STACK2STRING;
    builtin_map["strlen"] = node::STRLEN;
    builtin_map["strcat"] = node::STRCAT;
    builtin_map["char-at"] = node::CHAR_AT;
    builtin_map["chr"] = node::CHR;
    builtin_map["string"] = node::STRING;
    builtin_map["double"] = node::DOUBLE;
    builtin_map["parse"] = node::PARSE;
    builtin_map["type"] = node::TYPE;
    builtin_map["eval-string"] = node::EVAL_STRING;
    builtin_map["eval-list"] = node::EVAL_LIST;
    builtin_map["get"] = node::GET;
    builtin_map["set"] = node::SET;
    builtin_map["pr"] = node::PR;
    builtin_map["prn"] = node::PRN;
    builtin_map["clear"] = node::CLEAR;
    builtin_map["exit"] = node::EXIT;
}

vector<node> parse(string s) {
    s += ' '; // sentinel
    vector<node> ret;    
    string acc; // accumulator
    int pos = 0;
    int last = s.size() - 1;
    for (;pos <= last; pos++) {
        char c = s[pos];
        if (c == ' ' || c == '\t' || c == '\r' || c == '\n' || c == ';') {
            if (c == ';') { // end-of-line comment
                pos++;
                while (pos <= last && s[pos] != '\n') pos++;
            }            
            string tok = acc;
            acc = "";
            if (tok.size() == 0) continue;
            unordered_map<string, int>::iterator found = builtin_map.find(tok);
            if (found != builtin_map.end()) { // built-in operator
                ret.push_back(node::builtin(found->second));
            } else if (tok[0] == '\'') { // left-quoted string
                ret.push_back(node(tok.substr(1)));                
            } else if (tok[0] == '[') { // bracket-quoted string
                ret.push_back(node(tok.substr(1)));
            } else if (tok[0] == '(') { // list
                ret.push_back(node(parse(tok.substr(1))));
            } else if (isdigit(tok[0]) || tok[0] == '-' && isdigit(tok[1])) { // number
                ret.push_back(node(atof(tok.c_str())));
            } else { // variable get
                node n;
                n.type = node::T_VAR;
                n.v_string = tok;
                ret.push_back(n);
            }
        } else if (acc.size() == 0 && c == '[') { // beginning of string
            int numBracket = 1;
            acc += '[';
            pos++;
            while (true) {
                switch (s[pos]) {
                case '[': numBracket++; break;
                case ']': numBracket--; break;
                }
                if (numBracket <= 0) break;
                acc += s[pos];
                pos++;
            }
        } else if (acc.size() == 0 && c == '(') { // beginning of list
            int numBracket = 1;
            acc += '(';
            pos++;
            while (true) {
                switch (s[pos]) {
                case '(': numBracket++; break;
                case ')': numBracket--; break;
                }
                if (numBracket <= 0) break;
                acc += s[pos];
                pos++;
            }
        } else {
            acc += c;
        }
    }    
    return ret;
}

unordered_map<string, node> vars; // variables

node eval(vector<node> &lst) {    
    vector<node> stk;    
    for (vector<node>::iterator iter = lst.begin(); iter != lst.end(); iter++) {        
        switch (iter->type) {
        case node::T_BUILTIN: {
            switch (iter->v_builtin) {
            case node::PLUS: {
                double a2 = stk.back().v_double; stk.pop_back();
                double a1 = stk.back().v_double; stk.pop_back();                
                stk.push_back(node(a1 + a2)); break; }
            case node::MINUS: {
                double a2 = stk.back().v_double; stk.pop_back();
                double a1 = stk.back().v_double; stk.pop_back();
                stk.push_back(node(a1 - a2)); break; }
            case node::MUL: {
                double a2 = stk.back().v_double; stk.pop_back();
                double a1 = stk.back().v_double; stk.pop_back();
                stk.push_back(node(a1 * a2)); break; }
            case node::DIV: {
                double a2 = stk.back().v_double; stk.pop_back();
                double a1 = stk.back().v_double; stk.pop_back();
                stk.push_back(node(a1 / a2)); break; }
            case node::SQRT: {
                double a1 = stk.back().v_double; stk.pop_back();
                stk.push_back(node(sqrt(a1))); break; }
            case node::CARET: {
                double a2 = stk.back().v_double; stk.pop_back();
                double a1 = stk.back().v_double; stk.pop_back();
                stk.push_back(node(pow(a1, a2))); break; }
            case node::PERCENT: { // remainder
                double a2 = stk.back().v_double; stk.pop_back();
                double a1 = stk.back().v_double; stk.pop_back();
                stk.push_back(node((double) ((long long) a1 % (long long) a2))); break; }
            case node::EQEQ: {
                double a2 = stk.back().v_double; stk.pop_back();
                double a1 = stk.back().v_double; stk.pop_back();
                stk.push_back(node(a1 == a2)); break; }
            case node::NOTEQ: {
                double a2 = stk.back().v_double; stk.pop_back();
                double a1 = stk.back().v_double; stk.pop_back();
                stk.push_back(node(a1 != a2)); break; }
            case node::LT: {
                double a2 = stk.back().v_double; stk.pop_back();
                double a1 = stk.back().v_double; stk.pop_back();
                stk.push_back(node(a1 < a2)); break; }
            case node::GT: {
                double a2 = stk.back().v_double; stk.pop_back();
                double a1 = stk.back().v_double; stk.pop_back();
                stk.push_back(node(a1 > a2)); break; }
            case node::LTE: {
                double a2 = stk.back().v_double; stk.pop_back();
                double a1 = stk.back().v_double; stk.pop_back();
                stk.push_back(node(a1 <= a2)); break; }
            case node::GTE: {
                double a2 = stk.back().v_double; stk.pop_back();
                double a1 = stk.back().v_double; stk.pop_back();
                stk.push_back(node(a1 >= a2)); break; }
            case node::ANDAND: {
                double a2 = stk.back().v_double; stk.pop_back();
                double a1 = stk.back().v_double; stk.pop_back();
                stk.push_back(node(a1 && a2)); break; }
            case node::OROR: {
                double a2 = stk.back().v_double; stk.pop_back();
                double a1 = stk.back().v_double; stk.pop_back();
                stk.push_back(node(a1 || a2)); break; }
            case node::IF: { // pred (then-expr) (else-expr) if
                int len = stk.size();
                double pred = stk[len - 3].v_double;
                vector<node> expr;
                if (pred) {
                    expr = stk[len - 2].v_list;
                } else {
                    expr = stk[len - 1].v_list;
                }
                stk.pop_back();
                stk.pop_back();
                stk.pop_back();
                stk.push_back(eval(expr));
                break; }
            case node::WHEN: { // pred (then-expr) when
                int len = stk.size();                
                double pred = stk[len - 2].v_double;
                vector<node> expr;
                if (pred) {
                    expr = stk[len - 1].v_list;
                    stk.pop_back();
                    stk.pop_back();
                    stk.push_back(eval(expr));
                } else {
                    stk.pop_back();
                    stk.pop_back();
                }
                break; }
            case node::FOR: { // [var] start end step (expr) for
                vector<node> expr = stk.back().v_list; stk.pop_back();
                double step = stk.back().v_double; stk.pop_back();
                double end = stk.back().v_double; stk.pop_back();
                double start = stk.back().v_double; stk.pop_back();
                string var = stk.back().v_string; stk.pop_back();
                double &rvar = vars[var].v_double;
                if (start <= end) {
                    for (vars[var] = node(start); rvar <= end; rvar += step) {
                        eval(expr);
                    }
                } else {
                    for (vars[var] = node(start); rvar >= end; rvar += step) {
                        eval(expr);
                    }
                }
                break; }
            case node::WHILE: { // (condition) (expr) while
                vector<node> expr = stk.back().v_list; stk.pop_back();
                vector<node> cond = stk.back().v_list; stk.pop_back();
                while (eval(cond).v_double) {
                    eval(expr);
                }
                break; }
            case node::DUP: { // polymorphic
                node a1 = stk.back();
                stk.push_back(a1);
                break; }
            case node::SWAP: { // polymorphic
                node a2 = stk.back(); stk.pop_back();
                node a1 = stk.back(); stk.pop_back();
                stk.push_back(a2);
                stk.push_back(a1);
                break; }
            case node::POP: {
                stk.pop_back(); break; }
            case node::CLEAR: {
                stk.clear(); break; }
            case node::PLUSQ: { // sum of all
                double acc = 0.0;
                for (vector<node>::iterator iter = stk.begin(); iter != stk.end(); iter++)
                    acc += iter->v_double;
                stk.clear();
                stk.push_back(node(acc)); break; }
            case node::MULQ: { // product of all
                double acc = 1.0;
                for (vector<node>::iterator iter = stk.begin(); iter != stk.end(); iter++)
                    acc *= iter->v_double;
                stk.clear();
                stk.push_back(node(acc)); break; }
            case node::INC: {
                double a1 = stk.back().v_double; stk.pop_back();
                stk.push_back(node(a1 + 1)); break; }
            case node::DEC: {
                double a1 = stk.back().v_double; stk.pop_back();
                stk.push_back(node(a1 - 1)); break; }
            case node::PLUSPLUS: {
                string a1 = stk.back().v_string; stk.pop_back(); // variable
                vars[a1].v_double++; break; }
            case node::MINUSMINUS: {
                string a1 = stk.back().v_string; stk.pop_back(); // variable
                vars[a1].v_double--; break; }
            case node::FLOOR: {
                double a1 = stk.back().v_double; stk.pop_back();
                stk.push_back(node(floor(a1))); break; }
            case node::CEIL: {
                double a1 = stk.back().v_double; stk.pop_back();
                stk.push_back(node(ceil(a1))); break; }
            case node::LN: {
                double a1 = stk.back().v_double; stk.pop_back();
                stk.push_back(node(log(a1))); break; }
            case node::LOG10: {
                double a1 = stk.back().v_double; stk.pop_back();
                stk.push_back(node(log10(a1))); break; }
            case node::E: {
                stk.push_back(node(2.71828182845904523536)); break; }
            case node::PI: {
                stk.push_back(node(3.14159265358979323846)); break; }
            case node::RAND: {
                stk.push_back(node(rand_double())); break; }
            case node::PR: { // print
                node a1 = stk.back(); stk.pop_back();
                cout << a1.str(); break; }
            case node::PRN: { // println
                node a1 = stk.back(); stk.pop_back();
                cout << a1.str() << endl; break; }
            case node::EXIT: {
                puts("");
                exit(0); break; }
            case node::STACK2STRING: {
                stringstream ss2;
                for (vector<node>::iterator iter = stk.begin(); iter != stk.end(); iter++) {
                    ss2 << iter->str() << ' ';
                }
                stk.push_back(node(ss2.str()));
                break; }
            case node::STRING: {
                node a1 = stk.back(); stk.pop_back();
                stk.push_back(node(a1.str())); break; }
            case node::DOUBLE: {
                node a1 = stk.back(); stk.pop_back();
                stk.push_back(node(a1.dbl())); break; }
            case node::PARSE: {
                node a1 = stk.back(); stk.pop_back();
                stk.push_back(node(parse(a1.v_string))); break; }
            case node::EVAL_STRING: {
                string a1 = stk.back().v_string; stk.pop_back();
                stk.push_back(eval(parse(a1))); break; }
            case node::EVAL_LIST: {
                vector<node> a1 = stk.back().v_list; stk.pop_back();
                stk.push_back(eval(a1)); break; }
            case node::STRLEN: {
                node a1 = stk.back();
                string s = a1.v_string; stk.pop_back();
                stk.push_back(node(s.size())); break; }
            case node::STRCAT: {
                string a2 = stk.back().v_string; stk.pop_back();
                string a1 = stk.back().v_string; stk.pop_back();
                stk.push_back(node(a1 + a2)); break; }
            case node::CHAR_AT: {
                double a2 = stk.back().v_double; stk.pop_back();
                string a1 = stk.back().v_string; stk.pop_back();
                stk.push_back(node(a1[(unsigned int) a2])); break; }
            case node::CHR: {
                double a1 = stk.back().v_double;
                char temp[2] = " ";
                temp[0] = (char) a1;
                stk.push_back(node(string(temp))); break; }
            case node::TYPE: {
                node a1 = node(stk.back().type_str()); stk.pop_back();
                stk.push_back(a1); break; }
            case node::GET: { // example: 'a get
                string a1 = stk.back().v_string; stk.pop_back();
                unordered_map<string, node>::iterator found = vars.find(a1);
                if (found != vars.end()) {
                    stk.push_back(found->second);
                } else {
                    cout << "Unknown variable: [" << a1 << "]" << endl;
                }
                break; }
            case node::SET: { // example: 1 'a set
                string a2 = stk.back().v_string; stk.pop_back(); // variable
                node a1 = stk.back(); stk.pop_back(); // value
                vars[a2] = a1;
                break; }
            }
            break;
        } // end of case node::T_BUILTIN:
        case node::T_STRING:
        case node::T_DOUBLE:
        case node::T_LIST:
            {
                stk.push_back(*iter); break;
            }
        case node::T_VAR: // variable get
            {
                unordered_map<string, node>::iterator found = vars.find(iter->v_string);
                if (found != vars.end()) {
                    stk.push_back(found->second);
                } else {
                    cout << "Undefined variable: [" << iter->v_string << "]" << endl;
                }
                break;
            }            
        default:
            cout << "Unknown operator: [" << iter->str() << "]" << endl;
        }
    }
    if (stk.empty()) return node();    
    return stk.back();
}

void prompt() {
    printf("> ");
}

void prompt2() {
    printf("  ");
}

#define EP cout << eval(parse(line)).str_with_type() << endl;

// read-eval-print loop
void repl() {    
    while (true) {
        prompt();
        string line;
        if (!getline(cin, line)) {
            EP
            return;
        }
        string line2;
        while (true) {
            prompt2();
            if (!getline(cin, line2)) {
                EP
                return;
            }
            line += '\n' + line2;
            if (line2.size() == 0) {
                EP
                break;
            }
        }
    }
}

int main() {
    print_logo();
    init();
    repl();
    puts("");
}
